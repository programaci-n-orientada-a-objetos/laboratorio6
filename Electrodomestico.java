public class Electrodomestico {

		private String nombre;
		private boolean isOff;
		
		
		public Electrodomestico(String nombre, boolean isOff) { 
			  this.nombre = nombre;
			  this.isOff = isOff;
			}
		public String getNombre() { 
			  return nombre;
			}
	
			/** Set a new color */
			public void setNombre(String nombre) { 
			  this.nombre = nombre;
			}
	
			public boolean isisOff() { 
			  return isOff;
			}
	
			public void setOff(boolean isOff) { 
			  this.isOff = isOff;
			}
	
			@Override
			public String toString() {
			  return "El electrodomestico es: " + nombre + "¿esta apagado?" + isOff;
			}

}