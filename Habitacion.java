import java.util.ArrayList;

public class Habitacion {
	
	public String nombre;
	public ArrayList<Electrodomestico> electrodomesticos;
	
	Habitacion(String nombre, ArrayList<Electrodomestico> electrodomesticos) { 
		  this.nombre = nombre;
		  this.electrodomesticos = electrodomesticos;
	}
}